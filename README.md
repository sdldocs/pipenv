# What is Pipenv?
**Pipenv** is a production-ready tool that aims to bring the best of all packaging worlds to the Python world. It harnesses Pipfile, pip, and virtualenv into one single command.

It features very pretty terminal colors.

Source: [Pipenv & Virtual Environments](https://pipenv.pypa.io/en/latest/install/#)

# Pipenv란?
**Pipenv**는 Python에 모든 패키징 세계 중 최고의 제품을 제공하는 것을 목표로 하는 프로덕션용 도구입니다. `Pipfile`, `pip` 및 `virtualenv`를 하나의 명령어로 활용한다.

터미널 컬러가 매우 예쁜 것이 특징중 하나이다. 

Enjoy Python progrmming with Pipenv!

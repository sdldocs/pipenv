# Pipenv와 버추얼 환경
이 튜토리얼에서는 Python 패키지를 설치하고 사용하는 방법을 설명한다.

필요한 툴을 인스톨 해 사용하는 방법과 베스트 프랙티스에 관한 강력한 추천 방법에 대해 설명한다. Python은 매우 다양한 용도로 사용되므로 소프트웨어를 배포하는 방법에 따라 정확하게 종속성을 관리하는 방법이 달라질 수 있다. 여기서 가이드하는 방법는 네트워크 서비스(웹 애플리케이션 포함)의 개발 및 도입에 가장 직접 적용할 수 있을 뿐만 아니라 모든 종류의 프로젝트에 대한 개발 및 테스트 환경 관리에도 매우 적합하다.

> Note: <br>
> 이 가이드는 Python 3 기반으로 작성되어 있지만, 어떤 이유로 아직 사용 중인  Python 2.7에서도 정상적으로 동작한다.

- [설치된 Python과 pip을 확인](make-sure-your-python-and-pip.md)
- [Pipenv 설치](installing-pipenv.md)
- [프로젝트에 패키지 설치](installing-packages-for-your-project.md)
- [설치된 패키지 사용](using-installed-packages.md)
- [Virtualenv 매핑 절차](virtualenv-mapping-caveat.md)
- [다음 단계](next-step.md)

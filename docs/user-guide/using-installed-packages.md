# 설치된 패키지 사용
이제 Requests가 설치되었으므로 사용하기 위한 간단한 `main.py` 파일을 다음과 같이 만든다.
```python
import requests

response = requests.get('https://httpbin.org/ip')

print('Your IP is {0}'.format(response.json()['origin']))
```

그런 다음 `pipenv run`을 사용하여 이 스크립트를 실행할 수 있다.
```shell
$ pipenv run python main.py
```

아래와 유사한 결과를 얻을 수 있다.
```
Your IP is 8.8.8.8
```

`$ pipenv run`을 사용하면 설치된 패키지를 스크립트에서 사용할 수 있다. 또한 `$ pipenv shell`을 사용하여 모든 명령어가 설치된 패키지를 접근할 수 있는 새로운 쉘을 생성할 수도 있다.

# Pipenv 설치
Pipenv는 Python 프로젝트를 위한 종속성(dependency) 관리자이다. `Node.js`의 [npm]()이나 `Ruby`의 [bundler]()에 대해 알고 있다면, 이러한 툴의 정신과 비슷하다. `pip`은 Python 패키지를 설치할 수 있지만 일반적인 사용 사례에 대한 종속성 관리를 단순화하는 고급 도구이기 때문에 `Pipenv`를 권장한다.

## Pipx를 통한 Pipenv 분리 설치 <a id="isolated-installation-of-pipenv-with-pipx"></a>
[Pipx](https://pypa.github.io/pipx/)는 Python으로 작성된 최종 사용자 애플리케이션의 설치와 실행을 지원하는 도구이다. 애플리케이션을 격리된 깨끗한 환경에 직접 설치한다. `pipx`를 설치하려면 다음 작업을 수행한다.
```shell
$ pip install --user pipx
```

`pipx`가 설치되었으면 Pipenv 설치를 계속한다.
```shell
$ pip install pipenv
```

## 실용적 Pipenv 설치 <a id="pragmatic-installation-of-pipenv"></a>
pip을 정상적으로 설치하여 사용자 환경에서 글로벌 유틸리티로 특정 "툴 체인" 유형의 Python 모듈을 유지하는 경우, pip은 [user install]()로 홈 디렉토리에 설치할 수 있다. 종속성 간의 상호 작용으로 인해 `virtualenv`, `pipenv`, `tox` 및 유사한 소프트웨어를 Python 워크플로우의 기본 구성 요소로 이러한 방식으로 설치된 도구 사용에 제한이 있다.
```shell
$ pip install --user pipenv
```

> Note: <br>
> 시스템 전체의 패키지가 손상되는 것을 방지하기 위해 [사용자 설치(user installation)](https://pip.pypa.io/en/stable/user_guide/#user-installs))를 한다. 설치 후 쉘에서 pipenv를 사용할 수 없는 경우 [user base](https://docs.python.org/3/library/site.html#site.USER_BASE)의 바이너리 디렉토리를 `PATH`에 추가해야 한다.
> 
> Linux 및 macOS에서는 `python -m site --user-base`를 실행하고 끝에 `bin`을 추가하여 사용자 기반(user base) 바이너리 디렉토리에서 찾을 수 있다. 예를 들어, 보통 `~/.local` (`~`를 사용하여 홈 디렉토리의 절대 경로로 확장)을 출력하므로 `PATH`에 `~/.local/bin`를 추가해야 한다. `~/.profile`을 수정하여 `PATH`를 영구적으로 설정할 수 있다.
> 
> Windows에서 `python -m site --user-site`를 실행하고 `site-packages`를 `Scripts`로 바꾸면 사용자 기반 바이너리 디렉토리를 찾을 수 있다. 예를 들어 `C: C:\Users\Username\AppData\Roaming\Python38\site-packages`를 반환할 수 있고, `C:\Users\Username\AppData\로밍\Python38\Scripts`를 포함하도록 `PATH`를 설정해야 한다. [제어판](https://msdn.microsoft.com/en-us/library/windows/desktop/bb776899(v=vs.85).aspx)에서 사용자 `PATH`를 영구적으로 설정할 수 있다. `PATH` 변경 내용을 적용하려면 로그아웃해야 할 수 있다.
> 
> 상세한 것에 대하여는, [사용자 설치 문서](https://pip.pypa.io/en/stable/user_guide/#user-installs)를 참조하도록 한다.
> 

pipenv를 업그레이드하려면 언제든 다음 명령을 수행한다.
```shell
$ pip install --user --upgrade pipenv
```

## 소스로 Pipenv 설치 <a id="crude-installaction-of-pipenv"></a>
pip이 설치되어 있지 않은 경우 시스템 전체를 부트스트랩하는 다음과 같은 설치 방법을 사용할 수 있다.
```
$ curl https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py | python
```

## Homebrew로 Pipenv 설치 (비추) <a id="homebrew-installation-of-pipenv"></a>
[Homebrew](https://brew.sh/)는 많이 사용되는 MacOS용 오픈 소스 패키지 관리 시스템이다. Linux 사용자에게는 유사한 기능을 수행하는 [Linuxbrew](https://linuxbrew.sh/)가 있다.

Homebrew 또는 Linuxbrew를 통해 pipenv를 설치하면 pipenv 및 모든 종속성을 격리된 가상 환경에 유지하여 Python 설치를 방해하지 않는다.

Homebrew 또는 Linuxbrew를 설치한 후 다음 작업을 수행한다.
```
$ brew install pipenv
```

pipenv를 업그레이드하려면 언제든 다음 명령을 수행한다.
```shell
$ pip install --user --upgrade pipenv
```

> Note: <br>
> Homebrew 설치는 Pipenv가 의존하는 Homebrew Python을 업그레이드할 때마다 사용자는 Pipenv 및 Pipenv에 의해 관리되는 모든 가상 환경을 다시 설치해야 하므로 권장하지 않는다.

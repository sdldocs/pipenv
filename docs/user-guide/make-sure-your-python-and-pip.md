# 설치된 Python과 pip을 확인
더 나아가기 전에 사용하는 컴퓨터 시스템에 Python이 설치되어 있는지, 간단한 명령으로 사용할 수 있는지 어떤 버전인가를 확인한다. 이는 다음 명령으로 쉽게 확인할 수 있다.
```shell
$ python --version
```

이미 설치되어 있다면 아래와 같은 출력을 얻을 수 있습니다. Python이 설치되어 있지 않다면 (회사 서버에는 이미 설치되어 있다.) [python.org](https://python.org/)에서 최신 3.x 버전을 설치하거나 *The Hitchhiker's Guide to Python*의 [Installing Python](http://docs.python-guide.org/en/latest/starting/installation/)를 참조하도록 한다.
```
Python 3.8.10
```

> Note: <br>
> Python을 처음 접하는 경우라면 다음과 같은 오류가 발생할 수 있다.
> 
> ![python error](../images/fig-01.png)
> 
> 이는 이 명령어를 *shell*(*terminal* 또는 *console*이라고도 함)에서 실행하도록 되어 있기 때문이다. 운영 체제의 쉘 사용과 Python과 상호 작용하는 방법에 대한 소개는 Python 초보자를 위한  [Python for Beginners](https://opentechschool.github.io/python-beginners/en/index.html#) 페이지의 [Getting started](https://opentechschool.github.io/python-beginners/en/getting_started.html) 섹션을 참고하도록 한다.
> 

추가로 `pip`을 사용할 수 있는지 확인해야 한다. 다음을 실행하여 이를 확인할 수 있다.
```
$ pip --version
pip 22.0.3 from ...
```

[Homebrew](https://brew.sh/) 또는 [Linuxbrew](https://linuxbrew.sh/)를 이용하여 [python.org](https://python.org/)의 설치 프로그램으로 소스로 부터, Python을 설치했다면 pip이 이미 설치되어 있다. Linux에서 OS 패키지 매니저를 사용하여 설치한 경우 별도로 [pip을 설치](https://pip.pypa.io/en/stable/installing/)해야 할 수도 있다.

Homebrew 또는 Linuxbrew를 사용하여 Pipenv를 설치할 계획이라면 이 단계를 건너뛸 수 있다. Homebrew/Linuxbrew 설치 프로그램이 pip을 처리한다.

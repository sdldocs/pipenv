# Virtualenv 매핑 절차
- Pipenv는 자동으로 프로젝트를 특정 virtualenvs으로 매핑합니다.
- 프로젝트의 루트 디렉토리 이름과 프로젝트 루트에 대한 풀 경로의 해시(예: `my_project-a3de50`)를 사용하여 virtualenvfmf 글로벌하게 저장한다.
- 프로젝트의 경로를 변경하면 이러한 기본 매핑이 깨지고 pipenv는 더 이상 프로젝트의 virtualenv를 검색하여 사용할 수 없게 된다.
- 프로젝트 디렉토리 내에 virtualenv를 생성하기 위해 `.tmprc/.zshrc`(또는 임의의 셸 설정 파일)에 `export PIPENV_VENV_IN_PROJECT=1`을 설정하여 이후의 경로 변경 문제를 방지할 수 있다.

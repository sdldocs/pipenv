# 프로젝트에 패키지 설치
Pipenv는 프로젝트별로 종속성을 관리한다. 패키지를 설치하려면 프로젝트의 디렉터리(또는 이 튜토리얼의 빈 디렉터리)로 변경하고 다음을 실행한다.
```
$ cd myproject
$ pipenv install requests
```

> Note: <br>
> Pipenv는 권한이 없는 OS 사용자도 사용할 수 있도록 설계되었다. OS 전체에 설치하거나 다루지 않기 때문이다. OS가 제대로 작동하지 않을 수 있기 때문에 Pipenv를 `root` 또는 `sudo`(또는 Windows의 `Admin`)와 함께 실행하는 것을 절대로 권장하지 않는다.

Pipenv는 [Requests](https://docs.python-requests.org/en/latest/) 라이브러리를 설치하고 프로젝트 디렉토리에 `Pipfile`을 생성한다. `Pipfile`은 프로젝트를 다른 사용자와 공유할 때처럼 패키지를 재설치해야 하는 경우 프로젝트에 필요한 종속성을 추적하는 데 사용된다. 다음과 같이 출력된다 (단 출력된 정확한 경로는 다르다).
```
pipenv install requests
Creating a virtualenv for this project...
Pipfile: /home/user/myproject/Pipfile
sing /home/user/.local/share/virtualenvs/pipenv-Cv0J3wbi/bin/python3.9 (3.9.9) to create virtualenv...
 Creating virtual environment...created virtual environment CPython3.9.9.final.0-64 in 1142ms
 creator CPython3Posix(dest=/home/user/.local/share/virtualenvs/myproject-R3jRVewK, clear=False, no_vcs_ignore=False, global=False)
 seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/user/.local/share/virtualenv)
   added seed packages: pip==21.3.1, setuptools==60.2.0, wheel==0.37.1
 activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator

✔ Successfully created virtual environment!
Virtualenv location: /home/user/.local/share/virtualenvs/pms-R3jRVewK
Creating a Pipfile for this project...
Installing requests...
Adding requests to Pipfile's [packages]...
Installation Succeeded
Pipfile.lock not found, creating...
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
Building requirements...
Resolving dependencies...
✔ Success!
Updated Pipfile.lock (fe5a22)!
Installing dependencies from Pipfile.lock (fe5a22)...
🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 0/0 — 00:00:00
```
